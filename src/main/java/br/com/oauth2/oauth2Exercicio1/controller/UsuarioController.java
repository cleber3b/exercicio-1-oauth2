package br.com.oauth2.oauth2Exercicio1.controller;

import br.com.oauth2.oauth2Exercicio1.model.Usuario;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class UsuarioController {
    @GetMapping("/{nomeUsuario}")
    public Usuario crairUsuario(@PathVariable String nomeUsuario, @AuthenticationPrincipal Usuario usuario) {

        if (usuario.getNome().equals(nomeUsuario)) {
            usuario.setEmail("cleber3b@gmail.com");
            usuario.setTelefone("(11) 98878-8431");
            return usuario;
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Usuário não autorizado.");
        }
    }
}
