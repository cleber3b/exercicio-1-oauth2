package br.com.oauth2.oauth2Exercicio1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2Exercicio1Application {

	public static void main(String[] args) {
		SpringApplication.run(Oauth2Exercicio1Application.class, args);
	}

}
